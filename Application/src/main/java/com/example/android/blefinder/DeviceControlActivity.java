/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.blefinder;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okio.ByteString;

import static android.bluetooth.BluetoothDevice.ACTION_BOND_STATE_CHANGED;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private TextView mConnectionState;
    private TextView mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
    private final String LIST_CMT = "CMT";

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            } else if (BluetoothLeService.ACTION_RSSI_UPDATE.equals(action)) {
                int rssi = intent.getIntExtra("rssi", -1);
                if (rssi == -1) {
                    Log.v("woot", "warning no rssi update received.");
                }
                Toast.makeText(getApplicationContext(), "rssi = " + String.valueOf(rssi), Toast.LENGTH_SHORT).show();
                mBluetoothLeService.doRemoteRSSIRead();
            }
            else if (ACTION_BOND_STATE_CHANGED.equals(action)) {
                mBluetoothLeService.bondStateChanged();
            }
        }
    };

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            // TDL: Change this lines of code so that upon click it would:
            // write and read to the correct charactristic,
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLeService.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLeService.setCharacteristicNotification(
                                    characteristic, true);
                        }
                    return true;
                }
                return true;
                }};

    private final ExpandableListView.OnItemLongClickListener servicesLongClickListener =
            // TDL: Change this lines of code so that upon click it would:
            // write and read to the correct charactristic,
            new ExpandableListView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    int itemType = ExpandableListView.getPackedPositionType(id);
                    if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                        int childPosition = ExpandableListView.getPackedPositionChild(id);
                        int groupPosition = ExpandableListView.getPackedPositionGroup(id);

                        if (mGattCharacteristics != null) {
                            final BluetoothGattCharacteristic characteristic =
                                    mGattCharacteristics.get(groupPosition).get(childPosition);
                            final int charaProp = characteristic.getProperties();

                            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
                                // this property is writable, so let's try to write on it on click.
                                //
                                String ctype = SampleGattAttributes.lookup(characteristic.getUuid().toString() + "_type", null);
                                if (ctype.equals(SampleGattAttributes.GATTR_TYPE_HEX) || ctype == null) { // couldn't find type falling back to hex writing.
                                    Log.v("Woot", "Property is writable try to write it!");
                                    AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext());
                                    final boolean isPwdChange = (characteristic.getUuid().toString().equals(SampleGattAttributes.GATTR_UUID_PWD_CHANGE));
                                    if (isPwdChange) {
                                        builder.setTitle("Enter new password (6-digits only!):");
                                    } else {
                                        builder.setTitle("Edit characteristic (enter encoded hex value):");
                                    }
                                    // Set up the input
                                    final EditText input = new EditText(parent.getContext());
                                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                                    if (characteristic.getValue() == null)
                                        input.setText("");
                                    else
                                        input.setText(ByteString.of(characteristic.getValue()).hex());
                                    builder.setView(input);

                                    // Set up the buttons
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String s = input.getText().toString();
                                            if (s == null || s == "")
                                                return;
                                            if (s.length() % 2 != 0) {
                                                Log.v("woot", "invalid hex val");
                                                return;
                                            }
                                            for (char c : s.toCharArray()) {
                                                if (Character.digit(c, 16) == -1) {
                                                    Log.v("woot", "invalid hex val");
                                                    return;
                                                }
                                            }
                                            byte [] res = null;
                                            if (isPwdChange) {
                                                if (s.length() != 6 || !android.text.TextUtils.isDigitsOnly(s)) {
                                                    Log.v("woot", "password is invalid, its not 6 number chars");
                                                    return;
                                                }
                                                else {
                                                    // puts the number in a big-endian as in only 3-byte space
                                                    // first we will put it.
                                                    // modulize the passcode.
                                                    int int_passcode = Integer.parseInt(s) % (999999 + 1);
                                                    ByteBuffer b = ByteBuffer.allocate(4);
                                                    b.putInt(int_passcode);
                                                    res = Arrays.copyOfRange(b.array(), 1, 4);
                                                }
                                            }
                                            else {
                                                res = ByteString.decodeHex(s).toByteArray();
                                            }
                                            mBluetoothLeService.writeCharacteristic(characteristic, res);
                                        }
                                    });
                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    builder.show();
                                    // convert the text.
                                } else if (ctype.equals(SampleGattAttributes.GATTR_TYPE_BOOL)) {
                                    Log.v("woot", "changing bool attr: 00/01");
                                    final byte[] true_arr = {0x1};
                                    final byte[] false_arr = {0x0};
                                    AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext());
                                    builder.setTitle("Enable/Disable option");
                                    String msg = "Enable?";
                                    builder.setMessage(msg).setCancelable(false)
                                            .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    mBluetoothLeService.writeCharacteristic(characteristic, true_arr);
                                                    dialog.cancel();
                                                }
                                            })
                                            .setNegativeButton("Disable", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    mBluetoothLeService.writeCharacteristic(characteristic, false_arr);
                                                    dialog.cancel();
                                                }
                                            }).setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    }).show();

                                } else if (ctype.equals(SampleGattAttributes.GATTR_TYPE_INT_MODES)) {
                                    Log.v("woot", "changing int attr");
                                    // Set up the input
                                    String mode_max_str = SampleGattAttributes.lookup(characteristic.getUuid().toString() + "_mode_max", "-1");
                                    final int mode_max = Integer.parseInt(mode_max_str);
                                    if (mode_max == -1) {
                                        return true;
                                    }
                                    AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext());
                                    builder.setTitle("Enter mode:");
                                    // Set up the input
                                    final EditText input = new EditText(parent.getContext());
                                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                                    if (characteristic.getValue() == null)
                                        input.setText("");
                                    else
                                        input.setText(Integer.toString(ByteString.of(characteristic.getValue()).getByte(0)));
                                    builder.setView(input);

                                    // Set up the buttons
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String s = input.getText().toString();
                                            if (s == null || s == "")
                                                return;
                                            for (char c : s.toCharArray()) {
                                                if (Character.digit(c, 10) == -1) {
                                                    Log.v("woot", "invalid int val");
                                                    return;
                                                }
                                            }
                                            int mode = Integer.parseInt(s, 10);
                                            if (mode > mode_max) {
                                                Log.v("woot", "mode is too big.");
                                                return;
                                            }
                                            Log.v("woot", "writing mode byte");
                                            mBluetoothLeService.writeCharacteristic(characteristic, ByteBuffer.allocate(1).put((byte) Integer.parseInt(s)).array());
                                        }
                                    });
                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    builder.show();
                                    // convert the text.
                                } else if (ctype.equals(SampleGattAttributes.GATTR_TYPE_STRING)) {
                                    Log.v("woot", "changing string attr");
                                    AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext());
                                    builder.setTitle("Edit characteristic (enter string value):");

                                    // Set up the input
                                    final EditText input = new EditText(parent.getContext());
                                    // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                                    if (characteristic.getValue() == null)
                                        input.setText("");
                                    else
                                        input.setText(new String(characteristic.getValue()));
                                    builder.setView(input);

                                    // Set up the buttons
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String s = input.getText().toString();
                                            mBluetoothLeService.writeCharacteristic(characteristic, s.getBytes());
                                        }
                                    });
                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    builder.show();

                                }

                            }
                            displayGattServices(mBluetoothLeService.getSupportedGattServices());
                            return true;
                        }
                    }


                    //do your per-item callback here
                        return true; //true if we consumed the click, false if not

                    }
            };


    private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mGattServicesList.setOnItemLongClickListener(servicesLongClickListener);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        mDataField = (TextView) findViewById(R.id.data_value);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        // Bind our BluetoothLeService as a service to our activity.
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setEnabled(true);
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setEnabled(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_connect:
                item.setEnabled(false);
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                item.setEnabled(false);
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private void displayData(String data) {
        if (data != null) {
            mDataField.setText(data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        // TDL: Filter here the Gatt Services and display only the specfic intersted GATT-Services.
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            String serviceName = SampleGattAttributes.lookup(uuid, null);
            if (serviceName == null) {
                continue; // move over only known services.
            }
            currentServiceData.put(
                    LIST_NAME, serviceName);
            //currentServiceData.put(LIST_UUID, uuid);
            currentServiceData.put(LIST_CMT, SampleGattAttributes.lookup(uuid + "_comment", ""));
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            String charName;
            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                uuid = gattCharacteristic.getUuid().toString();
                charName = SampleGattAttributes.lookup(uuid, null);
                if (charName == null) {
                    continue; // move over only known charateristics.
                }
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_CMT, SampleGattAttributes.lookup(uuid + "_comment", ""));
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_CMT},
                new int[] { android.R.id.text1, android.R.id.text2 },
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_CMT},
                new int[] { android.R.id.text1, android.R.id.text2 }
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
        for (int i = 0; i < gattServiceData.size(); i++) {
            mGattServicesList.expandGroup(i);
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_RSSI_UPDATE);
        intentFilter.addAction(ACTION_BOND_STATE_CHANGED);
        return intentFilter;
    }
}
